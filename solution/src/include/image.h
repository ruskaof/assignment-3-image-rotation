#ifndef PICTURE_API_H
#define PICTURE_API_H

#include <stddef.h>
#include <stdint.h>

struct __attribute__((__packed__)) image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel {
    uint8_t r, g, b;
};

struct image* malloc_image(void);
void free_image(struct image* img);
struct pixel* malloc_pixels(size_t width, size_t height);

#endif
