#ifndef IMAGE_TRANSFORMER_PADDING_H
#define IMAGE_TRANSFORMER_PADDING_H

#include <stdio.h>

size_t get_padding(size_t width);

#endif
