#ifndef READER_H
#define READER_H

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    NOT_ENOUGH_MEMORY
};

#endif
