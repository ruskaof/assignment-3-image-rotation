#include "../include/image.h"
#include <stddef.h>

struct image rotate(struct image const source) {
    struct image rotated_image;
    rotated_image.width = source.height;
    rotated_image.height = source.width;
    rotated_image.data = malloc_pixels(rotated_image.width, rotated_image.height);

    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            rotated_image.data[j * rotated_image.width + (rotated_image.width - i - 1)] = source.data[i * source.width + j];
        }
    }

    return rotated_image;
}

