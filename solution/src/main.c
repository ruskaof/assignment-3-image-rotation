#include "include/config.h"
#include "include/reader_bmp.h"
#include "include/transforms.h"
#include <malloc.h>
#include <stdio.h>

char* cannot_open_file = "Cannot open file: %s";
char* cannot_read_file = "Cannot read file: %s";
char* cannot_write_file = "Cannot write file: %s";
char* cannot_close_file = "Cannot close file: %s";

int main(int argc, char** argv) {
    if (argc <= 2) {
        printf("Usage: <source-image> <transformed-image>");
        return 1;
    }

    if (debug) {
        printf("Source image: %s \t Transformed image: %s \n", argv[1], argv[2]);
    }

    const char* sourceImgName = argv[1];
    FILE* sourceImgFile = fopen(sourceImgName, "rb");
    if (sourceImgFile == NULL) {
        printf(cannot_open_file, sourceImgName);
        return 1;
    }
    struct image* image = malloc_image();
    enum read_status readStatus = from_bmp(sourceImgFile, image);
    int fclose_result = fclose(sourceImgFile);
    if (fclose_result != 0) {
        printf(cannot_close_file, sourceImgName);
        return 1;
    }

    if (readStatus != READ_OK) {
        printf("Error reading image: %s", sourceImgName);
        return 1;
    }

    if (debug) {
        FILE* debugFile = fopen("C:\\Users\\199-4\\Desktop\\ITMO\\labs\\pl\\lab3\\assignment-3-image-rotation\\my_tests\\output_test.bmp", "wb");
        to_bmp(debugFile, image);
    }

    const char* transformedImgName = argv[2];
    FILE* transformedImgFile = fopen(transformedImgName, "wb");
    if (transformedImgFile == NULL) {
        printf(cannot_open_file, transformedImgName);
        return 1;
    }
    struct image rotated_image = rotate(*image);
    enum write_status writeStatus = to_bmp(transformedImgFile, &rotated_image);
    fclose_result = fclose(transformedImgFile);
    if (fclose_result != 0) {
        printf(cannot_close_file, transformedImgName);
        return 1;
    }

    if (writeStatus != WRITE_OK) {
        printf(cannot_write_file, transformedImgName);
        return 1;
    }


    free_image(image);
    free(rotated_image.data);

    return 0;
}
