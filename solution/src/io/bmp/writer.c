#include "../../include/image.h"
#include "../../include/padding.h"
#include "../../include/reader.h"
#include "../../include/reader_bmp.h"
#include <stdio.h>


enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel) +
                       img->height * get_padding(img->width);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;


    size_t written_count = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (written_count != 1) {
        return WRITE_ERROR;
    }

    const size_t padding = get_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        written_count = fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        if (written_count != img->width) {
            return WRITE_ERROR;
        }
        for (size_t j = 0; j < padding; j++) {
            int fputc_result = fputc(0, out);
            if (fputc_result == EOF) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}

