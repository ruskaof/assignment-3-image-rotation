#include "../../include/config.h"
#include "../../include/image.h"
#include "../../include/padding.h"
#include "../../include/reader.h"
#include "../../include/reader_bmp.h"

#include <stdio.h>


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    size_t read_count = fread(&header, sizeof(struct bmp_header), 1, in);
    if (read_count != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != 0x4D42) {
        return READ_INVALID_HEADER;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc_pixels(img->width, img->height);

    if (img->data == NULL) {
        return NOT_ENOUGH_MEMORY;
    }

    const size_t padding = get_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        read_count = fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);
        if (read_count != img->width) {
            return READ_INVALID_BITS;
        }

        if (debug) {
            printf("read bits: %zu on the row %zu\n", read_count, i);
        }

        int fseek_result = fseek(in, (long) padding, SEEK_CUR);
        if (fseek_result != 0) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}
