#include "../../include/reader_bmp.h"
#include <malloc.h>

struct image* malloc_image(void) {
    struct image* img = (struct image*) malloc(sizeof(struct image));
    return img;
}

struct pixel* malloc_pixels(size_t width, size_t height) {
    struct pixel* pixels = (struct pixel*) malloc(sizeof(struct pixel) * width * height);
    return pixels;
}

void free_image(struct image* img) {
    free(img->data);
    free(img);
}
