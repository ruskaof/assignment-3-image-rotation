#include "../include/padding.h"

size_t get_padding(size_t width) {
    return (4 - (width * 3) % 4) % 4;
}
